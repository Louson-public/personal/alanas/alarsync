#!/bin/env python3

# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2024 Louis Rannou <louis.rannou@example.com>

import argparse
import logging
import signal
import sys

from . import alarsync


#: Default log level
default_log_level = 'info'


def create_parser():
    parser = argparse.ArgumentParser(
        description='alanas rsync daemon'
    )

    groupl = parser.add_argument_group("list", "list registered services")

    groupl.add_argument("-l", "--list",
                        action="store_true", dest="action_list",
                        help="Give the list of services registered")

    groupr = parser.add_argument_group("rsync command")

    groupr.add_argument("-d", "--debug",
                        action="store_const", const="debug", dest="log_level",
                        help="Enable debug logging (deprecated, use "
                             "--log-level debug).")
    groupr.add_argument("--log-level",
                        choices=["debug", "info", "warning", "error",
                                 "critical"],
                        default="%s" % (default_log_level),
                        help="Set log level (default: %s)" % default_log_level)

    groupr.add_argument("-o", "--options",
                        action="append", dest="rsync_opts",
                        help="Rsync options")
    groupr.add_argument("name", nargs="?", help="service name")

    return parser


def main(argv):
    parser = create_parser()

    args = parser.parse_args(argv)

    if args.log_level:
        logging.setLevelStr(args.log_level)

    if args.action_list:
        return alarsync.action_list()

    if not args.name:
        logging.error("error: the argument name is required")
        sys.exit(-1)

    try:
        return alarsync.action_rsync(args.name, opts=args.rsync_opts)
    except Exception as e:
        logging.error("error: {}".format(e))
        sys.exit(-1)


def interruption(signum, frame):
    signame = signal.Signals(signum).name
    logging.warn(f'Signal handler called with signal {signame}')
    sys.exit(0)


def entrypoint():
    signal.signal(signal.SIGINT, interruption)
    signal.signal(signal.SIGTERM, interruption)

    main(sys.argv[1:])
