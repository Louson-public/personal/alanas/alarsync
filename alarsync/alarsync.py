# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2024 Louis Rannou <louis.rannou@example.com>

import logging
import os
import subprocess


ALA_CONF_DIRS = [
    "~/.config/alanas",
    "/etc/alanas"
]


def config_find(confname="rsync"):
    """Find the config file. It may be placed in one the defaults directory or
    in a directory specified with the ALA_CONF_DIR environment variable. The
    config is named 'rsync' by default.
    """
    if "ALA_CONF_DIR" in os.environ:
        ala_dir = os.environ["ALA_CONF_DIR"]
        config_path = os.path.expanduser(os.path.join(ala_dir, confname))
        if os.path.exists(config_path):
            return config_path
        else:
            raise FileNotFoundError(
                "Config {} not found in {}".format(confname, ala_dir)
                )

    for d in ALA_CONF_DIRS:
        config_path = os.path.expanduser(os.path.join(d, confname))
        if os.path.exists(config_path):
            return config_path

    raise FileNotFoundError("config not found")


def config_read(confname="rsync"):
    """
    Read the config and return it as a dictrionary when it is found.
    """
    try:
        config = config_find(confname)

        ret = {}

        with open(config, "r") as f:
            for line in f.readlines():
                line = line.strip()
                if line.startswith('#'):
                    # This is a comment
                    continue

                line = line.split()

                if len(line) == 4:
                    key, src, dst, opts = line
                    src = src.replace("\\040", " ").replace("\\011", "\t")
                    dst = dst.replace("\\040", " ").replace("\\011", "\t")
                    if key in ret:
                        logging.warning("Warning: ignore duplicate, "
                                        "item is not unique: {}".format(key))
                    else:
                        opts = opts.split(",")
                        ret[key] = (src, dst,
                                    {"short": opts[0],
                                     "long": [o for o in opts[1:] if o != '']})
                elif len(line) == 3:
                    key, src, dst = line
                    src = src.replace("\\040", " ").replace("\\011", "\t")
                    dst = dst.replace("\\040", " ").replace("\\011", "\t")
                    if key in ret:
                        logging.warning("Warning: ignore duplicate, "
                                        "item is not unique: {}".format(key))
                    else:
                        ret[key] = (src, dst, {"short": "", "long": []})
                elif len(line) > 3:
                    logging.warning(
                        "Alanas rsync config: ignore malformatted line"
                    )

    except FileNotFoundError:
        logging.error("Error: Config not found")
        return {}

    return ret


def config_get_service(name, confname="rsync"):
    """
    Get configuration associated to a service name
    """
    c = config_read(confname)
    if name in c:
        return c[name]

    return None


def action_list(confname="rsync"):
    """
    Dump configuration
    """
    for key, items in config_read().items():
        print(key)
        print(" source:  {}".format(items[0] or ""))
        print(" dest:    {}".format(items[1] or ""))
        print(" options: {}".format(items[2] or ""))


def action_rsync(name, opts=None):
    """
    Apply rsync to service <name> according to the configuration
    """
    try:
        source, destination, s_opts = config_get_service(name)
    except KeyError as e:
        logging.error("No config found for {}".format(e))
        return -1
    except ValueError as e:
        logging.error("Failed to parse service line: {}".format(name))
        return -1

    command = ["rsync"]
    if s_opts["short"] != "":
        command.append("-" + s_opts["short"])
    command += ["--" + o for o in s_opts["long"]]
    if opts is not None:
        command += ["-" + o for o in opts]
    command += [source, destination]

    try:
        logging.info("Run rsync for {}".format(name))
        subprocess.check_output(command)
    except subprocess.CalledProcessError as e:
        logging.error("Error: rsync command failed {}".format(e))
        return -1

    return 0
