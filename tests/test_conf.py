# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2024 Louis Rannou <louis.rannou@louson.fr>

import os
import subprocess
import tempfile
import unittest

from alarsync import alarsync


class ConfReadTest(unittest.TestCase):
    def setUp(self):
        os.environ["ALA_CONF_DIR"] = "tests/testenv"

    def test_read_ok(self):
        ok_config = {
            "test1": ("source1", "dest1", {"short": "", "long": []}),
            "test2": ("source2", "dest2",
                      {"short": "shortopts2", "long": []}),
            "test3": ("source3", "dest3",
                      {"short": "shortopts3", "long": ["long3", "opts3"]}),
            "test4": ("source4", "dest4",
                      {"short": "", "long": ["long4", "opts4"]}),
            "test5": ("source5", "dest5",
                      {"short": "shortopts5", "long": []}),
            "test6": ("source6", "dest6", {"short": "", "long": []}),
            "testspace": ("source space", "destspace", {"short": "", "long": []}),
            "testspace2": ("sourcespace", "dest space", {"short": "", "long": []}),
            "testtab": ("source	tab", "desttab", {"short": "", "long": []}),
            "testtab2": ("sourcetab", "dest	tab", {"short": "", "long": []}),
        }
        c = alarsync.config_read()
        self.assertEqual(c, ok_config)

    def test_config_missing(self):
        c = alarsync.config_read("rsync_missing")
        self.assertEqual(c, {})

    def test_config_malformatted(self):
        c = alarsync.config_read("rsync_malformatted")
        self.assertEqual(c, {})

    def test_config_duplicate(self):
        dup_config = {
            "test1": ("source1", "dest1", {"short": "", "long": []})
        }
        c = alarsync.config_read("rsync_duplicate")
        self.assertEqual(c, dup_config)


class ConfGetTest(unittest.TestCase):
    def setUp(self):
        os.environ["ALA_CONF_DIR"] = "tests/testenv"

    def test_get_ok(self):
        s = alarsync.config_get_service("test2")
        self.assertEqual(s, ("source2", "dest2",
                             {"short": "shortopts2", "long": []})
                         )

    def test_get_config_missing(self):
        s = alarsync.config_get_service("test", "rsync_missing")
        self.assertIsNone(s)

    def test_get_missing(self):
        s = alarsync.config_get_service("testmissing")
        self.assertIsNone(s)

class TestCommands(unittest.TestCase):
    def setUp(self):
        self.olddir = os.getcwd()
        self.tmpdir = tempfile.TemporaryDirectory(prefix="tmp", dir="tests")

    def tearDown(self):
        os.chdir(self.olddir)
        self.tmpdir.cleanup()

    def test_list_ok(self):
        env = {"ALA_CONF_DIR": "tests/testenv"}
        process = subprocess.run(
            ["alarsync", "-l"],
            env=env
        )
        self.assertEqual(process.returncode, 0)

    def test_list_wrong_dir(self):
        env = {"ALA_CONF_DIR": "tests/testenv_FAKE"}
        process = subprocess.run(
            ["alarsync", "-l"],
            env=env
        )
        self.assertEqual(process.returncode, 0)

    def test_rsync_ok(self):
        data = "Is it a test sir ?"
        os.environ["ALA_CONF_DIR"] = os.path.abspath("tests/testenv")

        os.chdir(self.tmpdir.name)
        os.mkdir("source1")
        with open("source1/data", "w") as f:
            f.write(data)

        process = subprocess.run(
            ["alarsync", "-o", "absz", "test1"],
        )

        self.assertEqual(process.returncode, 0)
        self.assertTrue(os.path.exists("dest1/source1/data"))

        with open("dest1/source1/data", "r") as f:
            copy = f.read()
        self.assertEqual(copy, data)

    def test_rsync_wrong_dir(self):
        data = "Is it a test sir ?"
        os.environ["ALA_CONF_DIR"] = os.path.abspath("tests/testenv_FAKE")

        os.chdir(self.tmpdir.name)
        os.mkdir("source1")
        with open("source1/data", "w") as f:
            f.write(data)

        process = subprocess.run(
            ["alarsync", "-o", "absz", "test1"],
        )

        self.assertNotEqual(process.returncode, 0)


    def test_rsync_no_source(self):
        data = "Is it a test sir ?"
        os.environ["ALA_CONF_DIR"] = os.path.abspath("tests/testenv_FAKE")

        os.chdir(self.tmpdir.name)

        process = subprocess.run(
            ["alarsync", "-o", "absz", "test1"],
        )

        self.assertNotEqual(process.returncode, 0)
