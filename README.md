# Ala-rsync

Rsync wrapper for Alanas

Alarsync is a helper for rsync synchronization through systemd templated
services.

## License

BSD-2-Clause

## Installation

``` shell
python -m build
python -m installer
```

or with pip: `pip install alarsync`

## Configuration

alarsync requires a configuration file that must be placed in `~/.config/alanas`
or `/etc/alanas.d`. It may also be placed in a directory specified with the
environment variable `ALA_CONF_DIR`.

Each service is described on a separate line. Fields on each line are separated
by tabs or spaces. Lines starting with `#` are comments. Blank lines are
ignored.

The first field is a key name. Duplicated keys are ignored.

The second field is the source to synchronize as expected in a rsync command.

The third field is the destination of the rsync command.

The fourth field are optional rsync options. It should be made of comma
separated values. The first value matchs short options, the other values are
long options.

The following is a complete example with short and long options:

  `test source destination shortopts,longopt1,longopt2`

and will result to the rsync command:

  `rsync -shortopts --longopt1 --longopt2 source destination`

## Usage

alarsync can run rsync fo a given service:

``` shell
alarsync [-o RSYNC_SHORT_OPTS] <name>
```

It can list available services:

``` shell
alarsync -l
```

## As a systemd service

Systemd templated service and timer is provided in the resources directory. Run
`systemctl start alarsync@test` to run the previous rsync command or enable the
`alarsync@test.timer` to run it regularly.

By default, the service will be ran uner the alanas user/group. To specify an
other user, one may create `/usr/lib/systemd/system/alarsync@.service.d` with:

```
User=alanas
Group=alanas
```

